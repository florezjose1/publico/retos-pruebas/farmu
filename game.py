import random


class Game:
    def __init__(self) -> None:
        self.data_board = [
            [None, None, None],
            [None, "X", None],
            [None, None, None]
        ]
        self.options = ['X', '0']

    def print_board(self):
        """Draw the board"""
        print("    1 | 2 | 3 ")
        line = "  +---+---+---+"
        print(line)
        for i in range(len(self.data_board)):
            value_string = f"{i + 1} |"
            for value in self.data_board[i]:
                value_string += f" {value if value in self.options else '-'} |"

            print(value_string)
            print(line)

    def exist_positions(self):
        """Validated that there are still positions to continue playing"""
        exist = False
        for item in self.data_board:
            for v in item:
                if v is None:
                    exist = True
                    break

        return exist

    def winner(self):
        """Validated positions fill are equals indicating the winner"""
        value = None
        # Horizontal
        if self.data_board[0][0] == self.data_board[0][1] == self.data_board[0][2]:
            value = self.data_board[0][0]
        elif self.data_board[1][0] == self.data_board[1][1] == self.data_board[1][2]:
            value = self.data_board[1][0]
        elif self.data_board[2][0] == self.data_board[2][1] == self.data_board[2][2]:
            value = self.data_board[2][0]

        # Vertical
        elif self.data_board[0][0] == self.data_board[1][0] == self.data_board[2][0]:
            value = self.data_board[0][0]
        elif self.data_board[0][1] == self.data_board[1][1] == self.data_board[2][1]:
            value = self.data_board[0][1]
        elif self.data_board[0][2] == self.data_board[1][2] == self.data_board[2][2]:
            value = self.data_board[0][2]
        # Diagonal
        elif self.data_board[0][0] == self.data_board[1][1] == self.data_board[2][2]:
            value = self.data_board[0][0]
        elif self.data_board[2][0] == self.data_board[1][1] == self.data_board[0][2]:
            value = self.data_board[2][0]

        if value in self.options:
            return True, "Machine" if value == 'X' else 'Human'

        return False, None

    def game_machine(self):
        """Randomly, play for the machine"""
        x = random.randint(0, 2)
        y = random.randint(0, 2)
        played = False
        if not self.data_board[x][y]:
            self.data_board[x][y] = 'X'
            played = True

        if not played and self.exist_positions():
            self.game_machine()

    def capture_positions(self):
        """Capture positions of user (Human)"""
        try:
            position = str(
                input("Ingrese la coordenada de juego separada por coma (Horizontal, Vertical), ejemplo(1,1): "))
            positions = position.split(",")
            p_x = int(positions[0]) - 1
            p_y = int(positions[1]) - 1
        except Exception:
            print("Error, valores no válidos")
            self.capture_positions()

        return p_x, p_y

    @staticmethod
    def is_valid_positions(p_x: int, p_y: int) -> bool:
        """Validated range of positions is valid"""
        if p_x < 0 or p_x > 2 or p_y < 0 or p_y > 2:
            return False

        return True

    def play(self):
        self.print_board()
        exist_winner, who = self.winner()
        while not exist_winner:
            if not self.exist_positions():
                break
            p_x, p_y = self.capture_positions()

            if not self.is_valid_positions(p_x, p_y):
                print("Error, posición no válida")
                continue

            if not self.data_board[p_x][p_y]:
                self.data_board[p_x][p_y] = '0'
            else:
                print("Error, posición ya usada")
                continue

            self.print_board()
            exist_winner, who = self.winner()
            if exist_winner:
                break

            print("--" * 10, "Turn machine", "--" * 10)
            self.game_machine()
            self.print_board()
            exist_winner, who = self.winner()

        if exist_winner:
            print("\n\n", "**" * 40, "\n Ganador: ", who)
        else:
            print("\n\n", "**" * 40, "\n\n Parece que nadie ganó")


if __name__ == '__main__':
    game = Game()
    game.play()